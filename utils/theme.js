const scr = "only screen";
const mediaRange = {
  small: { min: 0, max: 640 },
  medium: { min: 641, max: 1024 },
  large: { min: 1025, max: 9999 },
};
const minWidth = (mediaRange) => `and (min-width: ${mediaRange.min}px)`;
const maxWidth = (mediaRange) => `and (max-width: ${mediaRange.max}px)`;

export const MEDIA_MEDIUM_UP = `${scr} ${minWidth(mediaRange.medium)}`;
export const MEDIA_LARGE_UP = `${scr} ${minWidth(mediaRange.large)}`;

export const colors = {
  border: "1px solid #00bfa1",
  backgroundColor: "#48696e",
};

export const btnMain = {
  backgroundColor: "#00bfa1",
  borderColor: " #00bfa1",
};

export const btnBack = {
  backgroundColor: "#f5992a",
  borderColor: "#f5992a",
};

export const txtWhite = {
  color: " #fff",
};

export const btnRound = {
  border: "none",
  borderRadius: "40px",
};

export const btnLine = {
  backgroundColor: "#29a744",
  color: " #fff",
  border: "none",
  borderRadius: "40px",
};

export const btnChat = {
  backgroundColor: "#00bfa0",
  color: " #fff",
  border: "none",
  borderRadius: "40px",
};

export const btnPhone = {
  backgroundColor: "#dd3546",
  color: " #fff",
  border: "none",
  borderRadius: "40px",
};
