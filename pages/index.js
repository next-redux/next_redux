import Head from "next/head";
import React, { useState, useEffect } from "react";
import { connect } from "react-redux";
import { loading } from "../redux/actions/loadingActions";
import {
  decrementCounter,
  incrementCounter,
} from "../redux/actions/counterActions";

import { Button } from "react-bootstrap";
import Layout from "../components/Layout";

function Home(props) {
  return (
    <div>
      <Head>
        <title>My Next</title>
        <meta
          name="viewport"
          content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no"
        />
      </Head>
      <Layout />
      <br />

      <Button onClick={() => props.loading(true)} variant="success">
        LOading On
      </Button>
      <Button onClick={() => props.loading(false)} variant="danger">
        LOading Off
      </Button>
    </div>
  );
}
const mapStateToProps = (state) => ({
  loadings: state.loading.status,
});

const mapDispatchToProps = {
  incrementCounter: incrementCounter,
  decrementCounter: decrementCounter,
  loading: loading,
};

export default connect(mapStateToProps, mapDispatchToProps)(Home);
