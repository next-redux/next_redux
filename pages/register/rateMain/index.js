import { MyCard } from "../../../public/styles/main";
// import { Table } from "react-bootstrap";

import rateThai from "../../../public/data/rate/thai.json";
import rateYee from "../../../public/data/rate/yeekee.json";
import rateHanoy from "../../../public/data/rate/hanoy.json";

export const ThaiRate = () => {
  return (
    <div>
      <div>
        <h4 style={{ color: "#00bfa1" }}>
          จ่ายสูงที่สุด<small> บาทละ</small> <span>900</span>
        </h4>
        <h5 style={{ color: "#fff" }}>หวยรัฐบาลไทย</h5>
      </div>
      <MyCard>
        <table id="rate">
          <thead>
            <tr>
              <th className="text-left">ประเภท</th>
              <th className="text-center">ราคา</th>
              <th className="text-right">อัตราจ่าย</th>
            </tr>
          </thead>
          {rateThai &&
            rateThai.map((res, index) => (
              <tbody key={res.id}>
                <tr>
                  <td className="text-left">{res.name}</td>
                  <td className="text-center">บาทละ</td>
                  <td className="text-right">{res.rate}</td>
                </tr>
              </tbody>
            ))}
        </table>
      </MyCard>
    </div>
  );
};

export const Yeekee = () => {
  return (
    <div>
      <div>
        <h4 style={{ color: "#00bfa1" }}>โปรโมชั่นจ่ายหนัก</h4>
        <h5 style={{ color: "#fff" }}>หวยยี่กี</h5>
      </div>
      <MyCard>
        <table id="rate">
          <thead>
            <tr>
              <th className="text-left">ประเภท</th>
              <th className="text-center">ราคา</th>
              <th className="text-right">อัตราจ่าย</th>
            </tr>
          </thead>
          {rateYee &&
            rateYee.map((res, index) => (
              <tbody key={res.id}>
                <tr>
                  <td className="text-left">{res.name}</td>
                  <td className="text-center">บาทละ</td>
                  <td className="text-right">{res.rate}</td>
                </tr>
              </tbody>
            ))}
        </table>
      </MyCard>
    </div>
  );
};

export const Hanoy = () => {
  return (
    <div>
      <div>
        <h4 style={{ color: "#00bfa1" }}>โปรโมชั่นจ่ายหนัก</h4>
        <h5 style={{ color: "#fff" }}>หวยฮานอย หวยลาว หวยมาเลเซีย หวยหุ้น</h5>
      </div>
      <MyCard>
        <table id="rate">
          <thead>
            <tr>
              <th className="text-left">ประเภท</th>
              <th className="text-center">ราคา</th>
              <th className="text-right">อัตราจ่าย</th>
            </tr>
          </thead>
          {rateHanoy &&
            rateHanoy.map((res, index) => (
              <tbody key={res.id}>
                <tr>
                  <td className="text-left">{res.name}</td>
                  <td className="text-center">บาทละ</td>
                  <td className="text-right">{res.rate}</td>
                </tr>
              </tbody>
            ))}
        </table>
      </MyCard>
    </div>
  );
};
