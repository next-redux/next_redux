import React, { useState, useEffect, useCallback } from "react";

import { Form, Button, InputGroup, FormControl } from "react-bootstrap";
import Layout from "../../components/Layout";
import { btnMain, btnBack } from "../../utils/theme";
import {
  MyCard,
  MyCardHeader,
  MyCardCont,
  MyButtonV1,
} from "../../public/styles/main";

import Contact from "../../components/Contact";
import { ThaiRate, Yeekee, Hanoy } from "./rateMain";
import StockRate from "./resultStock";

export default () => {
  const [code, setCode] = useState(1234);

  const generateCode = useCallback(async () => {
    let items = ["0", "1", "2", "3", "4", "5", "6", "7", "8", "9"];
    let r1 = Math.floor(Math.random() * 10);
    let r2 = Math.floor(Math.random() * 10);
    let r3 = Math.floor(Math.random() * 10);
    let r4 = Math.floor(Math.random() * 10);

    let code = (await items[r1]) + items[r2] + items[r3] + items[r4];

    setCode(code);
  }, []);

  useEffect(() => {
    generateCode();
  }, [generateCode]);

  return (
    <Layout>
      <MyCard>
        <MyCardHeader style={{ backgroundColor: "#f7f7f7" }}>
          <h2 style={{ fontWeight: "bolder" }}>สมัครสมาชิก</h2>
        </MyCardHeader>
        <MyCardCont>
          <p className="textWraning" style={{ fontSize: "12px" }}>
            กรุณากรอกเฉพาะข้อมูลจริงเท่านั้นเพื่อประโยชน์ของตัวท่านเอง
          </p>
        </MyCardCont>

        <MyCardCont>
          <InputGroup className="mb-3">
            <InputGroup.Prepend>
              <InputGroup.Text id="basic-addon1">Mobile</InputGroup.Text>
            </InputGroup.Prepend>
            <FormControl
              placeholder="กรุณาใส่หมายเลขโทรศัพท์เพื่อยืนยันตัวตน"
              aria-label="Username"
              aria-describedby="basic-addon1"
            />
          </InputGroup>
        </MyCardCont>

        <MyCardCont>
          <hr style={{ background: " #00bfa1" }} />
          <h5 style={{ color: "#00bfa1" }}>กรุณากรอกตัวเลข</h5>
        </MyCardCont>

        <MyCardCont>
          <Form.Control
            className="mb-1 pa-4 text-center textWraning"
            style={{ letterSpacing: " 8px" }}
            size="lg"
            type="number"
            value={code}
            disabled
          />
          <Form.Control size="lg" type="number" placeholder="กรุณากรอกตัวเลข" />
        </MyCardCont>

        <MyCardCont>
          <MyButtonV1 style={btnMain}>ถัดไป</MyButtonV1>
          <MyButtonV1 style={btnBack}>มีไอดีแล้วกดที่นี่</MyButtonV1>
        </MyCardCont>
      </MyCard>

      <Contact />
      <ThaiRate />
      <Yeekee />
      <Hanoy />

      <StockRate />
    </Layout>
  );
};
