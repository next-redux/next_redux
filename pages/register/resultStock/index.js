import { MyCard, MyCardHeader } from "../../../public/styles/main";
import LtRes from "../../../public/data/result/stock.json";

const StockRate = () => {
  return (
    <div>
      <MyCard>
        <MyCardHeader style={{ background: " #2f3e43" }}>
          <h5 className="text-white">
            <b>หวยหุ้นต่างประเทศ</b>
          </h5>
        </MyCardHeader>
        <MyCardHeader style={{ background: " #57646a" }}>
          <h5 className="text-light">16 มิถุนายน 2563</h5>
        </MyCardHeader>
      </MyCard>

      {LtRes &&
        LtRes.map((res, index) => (
          <MyCard key={res.id}>
            <MyCardHeader style={{ background: " #2f3e43" }}>
              <h5 className="text-white">
                <b>{res.name}</b>
              </h5>
            </MyCardHeader>
            <div>
              <table id="resultLt">
                <thead>
                  <tr>
                    <th>{res.jackpot[0].name}</th>
                    <th>{res.jackpot[1].name}</th>
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <td>{res.jackpot[0].num}</td>
                    <td>{res.jackpot[1].num}</td>
                  </tr>
                </tbody>
              </table>
            </div>
          </MyCard>
        ))}
    </div>
  );
};

export default StockRate;
