import { combineReducers } from "redux";

import counterReducer from "./counterReducer";
import loadingReducer from "./loadingReducer";

const rootReducer = combineReducers({
  counter: counterReducer,
  loading: loadingReducer,
});

export default rootReducer;
