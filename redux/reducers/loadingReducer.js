import { loading } from "../actions/loadingActions";

const loadingReducer = (state = { value: false }, action) => {
  switch (action.type) {
    case "LOADING_ON":
      console.log("T");
      return { ...state, value: (state.value = true) };
    case "LOADING_OFF":
      console.log("F");
      return { ...state, value: (state.value = false) };
    default:
      console.log("Default");
      return { ...state };
  }
};

export default loadingReducer;
