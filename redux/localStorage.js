import backList from './backListStore'


const loadState = () => {
    try {
        const serializedState = localStorage.getItem('store')
        // console.log('serializedState', serializedState);

        if (serializedState === null) {
            return undefined
        } else {
            return JSON.parse(serializedState)
        }
    } catch (error) {
        return undefined
    }
}

const saveState = (state) => {
    try {
        let storeState = {};
        if (backList.length > 0) {
            for (const property in state) {
                const everyResult = backList.some((backList) => backList == property)
                if (!everyResult) {
                    storeState[property] = { ...state[property] };
                }
            }
        } else {
            storeState = { ...state }
        }
        const serializedState = JSON.stringify(storeState)
        localStorage.setItem('store', serializedState)
    } catch (error) {
        console.log(error.message)
    }
}

export {
    loadState,
    saveState,
}