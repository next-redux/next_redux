export const LOADING_ON = "LOADING_ON";
export const LOADING_OFF = "LOADING_OFF";

//Action Creator
export const loading = (value) => {
  console.log("V: ", value);
  if (value === true) {
    return {
      type: LOADING_ON,
    };
  } else {
    return {
      type: LOADING_OFF,
    };
  }
};

// export const loadingOn = () => ({
//   type: LOADING_ON,
// });

// export const loadingOff = () => ({
//   type: LOADING_OFF,
// });
