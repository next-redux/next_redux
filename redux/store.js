import { createStore } from "redux";
import rootReducer from "./reducers/rootReducer";
import { loadState, saveState } from './localStorage';

const persistStore = loadState()
const store = createStore(rootReducer, persistStore)

store.subscribe(() => {
    saveState(store.getState())
})

// const store = createStore(rootReducer);

export default store;



