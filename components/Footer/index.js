import { MyFooter } from "../../public/styles/footer";

const Footer = () => {
  return (
    <div>
      <MyFooter>
        <span>2016 COPYRIGHT - HUAYLIKE.COM</span>
      </MyFooter>
    </div>
  );
};

export default Footer;
