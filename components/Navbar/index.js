import React, { useState } from "react";
import Link from "next/link";

import { MyNavBar, MyBurger } from "../../public/styles/navbar";
import menuList from "../../public/data/menu";

export default () => {
  const [menu, setMenu] = useState(false);

  const menuOnOff = () => {
    setMenu(!menu);
  };

  const listItems = menuList.map((m) => (
    <li key={m.name}>
      <Link href={m.url}>
        <a>{m.name}</a>
      </Link>
    </li>
  ));

  return (
    <MyNavBar>
      <MyBurger onClick={menuOnOff}>
        <div className={`bar1 ${menu ? "change1" : ""}`}></div>
        <div className={`bar2 ${menu ? "change2" : ""}`}></div>
        <div className={`bar3 ${menu ? "change3" : ""}`}></div>
      </MyBurger>

      <div className={`menuM ${menu ? "" : "hidden"}`}>
        <ul>{listItems}</ul>
      </div>
      {/* <div onClick={menuOnOff} className="overlayMenu"></div> */}
    </MyNavBar>
  );
};
