import React, { useState, useEffect } from "react";
import Link from "next/link";
import { library } from "@fortawesome/fontawesome-svg-core";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { fab } from "@fortawesome/free-brands-svg-icons";
import { fas } from "@fortawesome/free-solid-svg-icons";

library.add(fas);

export default (props) => {
  const [checkSync, setCheckSync] = useState(false);

  const clickSyncFun = () => {
    setCheckSync(true);
    setTimeout(() => setCheckSync(false), 3000);
  };

  const checkSyncFun = () =>
    !checkSync ? (
      <FontAwesomeIcon icon="sync" />
    ) : (
      <FontAwesomeIcon icon="sync" spin />
    );

  useEffect(() => {
    checkSyncFun();
  }, [checkSyncFun]);

  // beforn login
  // return (
  //     <nav id="navbarTopMenu" classNameName="navbar p-0 ">
  //         <div classNameName="container-fluid">
  //             <form classNameName="form-inline pl-3">

  //             </form>
  //             <form classNameName="form-inline px-3 m-0">
  //                 <Link href="/" id="navbarBrandCoverLogo" classNameName="navbar-brand w-auto mr-0 py-0">
  //                     <img classNameName="logo m-auto" src="https://huaylike.com/images/0_imgfordev/img_logo01.png" data-toggle="tooltip" alt="HUAYLIKE.COM" title="" id="logofull" data-original-title="HUAYLIKE.COM" />
  //                 </Link>
  //             </form>
  //             <button classNameName="navbar-toggler" style={{ border: 'unset' }} type="button"></button>
  //         </div >

  //     </nav >
  // );

  // after login
  return (
    <nav id="navbarTopMenu" className="navbar p-0 ">
      <div className="container-fluid px-0">
        <form
          className="form-inline pl-3"
          style={{ width: "60%", position: "absolute" }}
        >
          <Link href="/">
            <img
              id="logo-after-login"
              src="https://front-dev.huayking.com/images/0_imgfordev/img_logo01.png"
              data-toggle="tooltip"
              alt="HUAYLIKE.COM"
              title=""
              className="d-inline-block align-top d-block d-md-none"
              style={{ width: "85%" }}
              data-original-title="HUAYLIKE.COM"
            />
          </Link>
        </form>
        <form
          className="form-inline"
          style={{ position: "absolute", right: "0px" }}
        >
          <button
            type="button"
            onClick={() => clickSyncFun()}
            id="sync-balanceBonut"
            data-toggle="tooltip"
            title=""
            className="navbar-toggler text-white"
            style={{ border: "unset" }}
            data-original-title="รีเฟรชยอดเครดิต"
          >
            {checkSyncFun()}
          </button>
          <span
            data-toggle="tooltip"
            title=""
            id="balanceBonut-app"
            className="bg-white my-auto credit-balance"
            data-original-title="ยอดเครดิต"
          >
            202,387,405.40 ฿
          </span>
          <button
            id="sidebarCollapse"
            type="button"
            data-toggle="tooltip"
            title=""
            className="navbar-toggler text-white"
            style={{ border: "unset" }}
            data-original-title="เมนู"
          >
            <FontAwesomeIcon icon="bars" />
          </button>
        </form>
      </div>
    </nav>
  );
};
