import { connect } from "react-redux";

const Loading = (props) => {
  // console.log("PROPS: ", props);

  return (
    <div></div>
    // <div className={`mainLoading ${props.loadings ? "" : "hidden"}`}>
    //   <div className="loader" />
    //   <div className="overlay" />
    // </div>
  );
};

const mapStateToProps = (state) => ({
  loadings: state.loading.value,
});

export default connect(mapStateToProps)(Loading);
