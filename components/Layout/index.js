import Navbar from "../Navbar";
import Loading from "../Loading";
import Marquee from "../Marquee";
import Header from "../Header";
import Footer from "../Footer";

export default function Layout(props) {
  return (
    <div>
      <Marquee />
      <Header />

      {props.children}

      <Loading />
      <Footer />
    </div>
  );
}
