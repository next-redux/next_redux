import { MyCard, MyCardHeader, MyCardCont } from "../../public/styles/main";
import { Btn } from "../../public/styles/button";
import { btnLine, btnChat, btnPhone } from "../../utils/theme";

const Contact = () => {
  return (
    <div>
      <MyCard>
        <MyCardHeader style={{ backgroundColor: "#2f3e43" }}>
          <h4 style={{ color: "#fff" }}>เลือกช่องทางการติดต่อเรา</h4>
        </MyCardHeader>
        <MyCardCont>
          <Btn style={btnLine}>ติดต่อผ่านไลน์</Btn>
          <Btn style={btnChat}>แชทสดกับพนักงาน</Btn>
          <Btn style={btnPhone}>ติดต่อผ่านโทรศัพท์</Btn>
        </MyCardCont>
      </MyCard>
    </div>
  );
};

export default Contact;
