import styled from "styled-components";
import { MEDIA_MEDIUM_UP } from "../../utils/theme";

export const MyNavBar = styled.div`
  height: 60px;
  width: 100%;
  background-color: gray;

  @media ${MEDIA_MEDIUM_UP} {
    height: 48px;
    width: 100%;
    background-color: orange;
  }
`;

export const MyBurger = styled.div`
  position: absolute;
  top: 8px;
  left: 16px;
  display: inline-block;
  cursor: pointer;
  z-index: 30;

  @media ${MEDIA_MEDIUM_UP} {
    display: none;
  }
`;

export const MenuLaptop = styled.div`
  display: none;

  @media ${MEDIA_MEDIUM_UP} {
  }
`;
