import styled from "styled-components";
import { MEDIA_MEDIUM_UP } from "../../utils/theme";

export const MyCard = styled.div`
  width: 90%;
  margin: 16px;
  background-color: #ffffff;

  @media ${MEDIA_MEDIUM_UP} {
  }
`;

export const MyCardCont = styled.div`
  padding: 8px 16px;
  background-color: #ffffff;

  @media ${MEDIA_MEDIUM_UP} {
  }
`;

export const MyCardHeader = styled.div`
  font-size: 24px;
  padding: 4px;
  font-weight: bolder;
  color: #000;

  @media ${MEDIA_MEDIUM_UP} {
    font-size: 24px;
    padding: 8px;
    font-weight: bolder;
    color: green;
  }
`;

export const MyButtonV1 = styled.button`
  font-size: 16px;
  padding: 8px 15px;
  margin: 8px 0px;
  width: 100%;
  display: inline-block;
  transform: translate(0);
  overflow: hidden;
  color: #000;
  text-align: center;
  text-transform: uppercase;
  text-decoration: none;
`;
