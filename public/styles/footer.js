import styled from "styled-components";
import { MEDIA_MEDIUM_UP } from "../../utils/theme";

export const MyFooter = styled.div`
  border-top: solid 1px #000;
  background: #012b29;
  background: linear-gradient(49deg, #012b29 0%, #015347 100%);
  filter: progid:DXImageTransform.Microsoft.gradient(startColorstr="#012b29",endColorstr="#015347",GradientType=1);
  text-align: center;
  padding: 8px 0px;
  color: #fff;

  @media ${MEDIA_MEDIUM_UP} {
    border-top: solid 1px #000;
    background: #012b29;
    background: linear-gradient(49deg, #012b29 0%, #015347 100%);
    filter: progid:DXImageTransform.Microsoft.gradient(startColorstr="#012b29",endColorstr="#015347",GradientType=1);
    text-align: center;
    padding: 8px 0px;
    color: #fff;
  }
`;
