import styled from "styled-components";
import { MEDIA_MEDIUM_UP } from "../../utils/theme";

export const Btn = styled.button`
  width: -webkit-fill-available;
  margin: 8px 0px;
  padding: 8px;
  font-size: 1.2em;

  @media ${MEDIA_MEDIUM_UP} {
  }
`;
